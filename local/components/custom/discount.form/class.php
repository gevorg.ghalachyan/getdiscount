<?php

use \Bitrix\Main\Localization\Loc as Loc;
use \Bitrix\Main\SystemException as SystemException;
use \Bitrix\Main\Loader as Loader;
use \Bitrix\Sale\DiscountCouponsManager;
use \Bitrix\Sale\Internals\DiscountCouponTable;
use \Bitrix\Sale\Internals\DiscountTable;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

class UserDiscountGenerator extends CBitrixComponent
{
    const IBLOCK_CODE = 'discount';
    protected $isNewDiscount = false;

    /**
     * @param $params
     * @override
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        global $USER;
        $params['IBLOCK_ID'] = \Bitrix\Iblock\IblockTable::getList(['filter'=>['CODE'=>self::IBLOCK_CODE]])->Fetch()["ID"];;
        $params['DISCOUNT_VALUE'] = rand(1, 50);
        $params['USER_ID'] = $USER->GetID();
        $params['DISCOUNT_UNIT'] = 'Perc';
        return $params;
    }

    /**
     * @override
     * @throws Exception
     */
    protected function checkModules()
    {
        if (!Loader::includeModule("iblock"))
            throw new SystemException(Loc::getMessage("DF_IBLOCK_MODULE_NOT_INSTALLED"));
        if (!Loader::includeModule("sale"))
            throw new SystemException(Loc::getMessage("DF_SALE_MODULE_NOT_INSTALLED"));
        if (!Loader::includeModule("catalog"))
            throw new SystemException(Loc::getMessage("DF_CATALOG_MODULE_NOT_INSTALLED"));
    }

    /**
     *
     */
    protected function getUserDiscount()
    {
        $this->arResult = [];
        $arSelect = array("ID", "PROPERTY_DISCOUNT_ADD_DATE", "PROPERTY_COUPON", "PROPERTY_DISCOUNT");
        $arFilter = array("IBLOCK_ID" => $this->arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_USER" => $this->arParams['USER_ID']);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 1), $arSelect);
        if ($arField = $res->fetch()) {
            $discountAddDate = strtotime($arField['PROPERTY_DISCOUNT_ADD_DATE_VALUE']);
            if ((time() - $discountAddDate) / 3600 <= 1) {
                $this->arResult = [
                    "DISCOUNT" => $arField['PROPERTY_DISCOUNT_VALUE'],
                    "DISCOUNT_TEXT" => Loc::getMessage('DF_DISCOUNT_TEXT') . $arField['PROPERTY_DISCOUNT_VALUE'] . '%',
                    'COUPON' => $arField['PROPERTY_COUPON_VALUE'],
                    'COUPON_TEXT' => Loc::getMessage('DF_COUPON_TEXT') . $arField['PROPERTY_COUPON_VALUE']
                ];
            } else {
                $arrCoupon = DiscountCouponsManager::getData($arField['PROPERTY_COUPON_VALUE']);

                DiscountTable::delete($arrCoupon['DISCOUNT_ID']);

                $result = DiscountCouponTable::delete($arrCoupon['ID']);
                if (!$result->isSuccess())
                    $this->arResult['ERRORS'] = implode('\n', $result->getErrorMessages());
                else
                    $this->arResult['ERRORS'] = Loc::getMessage('DF_COUPON_DELETED');
            }
        }
    }

    /**
     *
     */
    protected function checkUserCoupon($coupon = '')
    {
        $this->arResult = [];
        $arSelect = array("ID", "PROPERTY_DISCOUNT_ADD_DATE", "PROPERTY_COUPON", "PROPERTY_DISCOUNT");
        $arFilter = array("IBLOCK_ID" => $this->arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_USER" => $this->arParams['USER_ID'], "PROPERTY_COUPON" => $coupon);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 1), $arSelect);
        if ($arField = $res->fetch()) {
            $discountAddDate = strtotime($arField['PROPERTY_DISCOUNT_ADD_DATE_VALUE']);
            if ((time() - $discountAddDate) / 3600 < 3) {
                $this->arResult = [
                    "DISCOUNT" => $arField['PROPERTY_DISCOUNT_VALUE'],
                    "DISCOUNT_TEXT" => Loc::getMessage('DF_DISCOUNT_TEXT') . $arField['PROPERTY_DISCOUNT_VALUE'] . '%',
                ];
            } else {
                $this->arResult['ERRORS'] = Loc::getMessage('DF_IBLOCK_COUPON_ERROR');
            }
        } else {
            $this->arResult['ERRORS'] = Loc::getMessage('DF_IBLOCK_COUPON_ERROR');
        }
    }

    /**
     * @return int
     */
    protected function getUserDiscountItemId()
    {
        $arSelect = array("ID");
        $arFilter = array("IBLOCK_ID" => $this->arParams['IBLOCK_ID'], "ACTIVE" => "Y", "PROPERTY_USER" => $this->arParams['USER_ID']);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 1), $arSelect);
        if ($arField = $res->fetch()) {
            return $arField['ID'];
        }
        return 0;
    }

    /**
     * @return boolean
     */
    protected function addUserDiscount($coupon = '')
    {
        if (!$coupon)
            return false;

        $el = new CIBlockElement;
        $PROP = [];
        $PROP['USER'] = $this->arParams['USER_ID'];
        $PROP['COUPON'] = $coupon;
        $PROP['DISCOUNT'] = $this->arParams['DISCOUNT_VALUE'];
        $PROP['DISCOUNT_ADD_DATE'] = ConvertTimeStamp(time(), "FULL");

        $arLoadProductArray = array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "PROPERTY_VALUES" => $PROP,
            "NAME" => Loc::getMessage('DF_IBLOCK_ITEM_PRE_NAME') . $this->arParams['USER_ID'],
            "ACTIVE" => "Y",
        );

        $userItemId = $this->getUserDiscountItemId();
        if($userItemId) {

            $this->arResult = [
                "DISCOUNT" => $this->arParams['DISCOUNT_VALUE'],
                "DISCOUNT_TEXT" => Loc::getMessage('DF_DISCOUNT_TEXT') . $this->arParams['DISCOUNT_VALUE'] . '%',
                'COUPON' => $coupon,
                'COUPON_TEXT' => Loc::getMessage('DF_COUPON_TEXT') . $coupon
            ];
            return $el->Update($userItemId, $arLoadProductArray);
        } else {
            if ($el->Add($arLoadProductArray)) {

                $this->arResult = [
                    "DISCOUNT" => $this->arParams['DISCOUNT_VALUE'],
                    "DISCOUNT_TEXT" => Loc::getMessage('DF_DISCOUNT_TEXT') . $this->arParams['DISCOUNT_VALUE'] . '%',
                    'COUPON' => $coupon,
                    'COUPON_TEXT' => Loc::getMessage('DF_COUPON_TEXT') . $coupon
                ];
                return true;
            }
        }
    }

    /**
     *
     * @return Json
     */
    protected function prepareData()
    {
        global $APPLICATION;
        $post = \Bitrix\Main\HttpRequest::getInput();
        if ($post) {
            $this->arResult = [];
            $post = \Bitrix\Main\Web\Json::decode($post, true);
            if (isset($post['AJAX']) && $post['AJAX'] == 'Y') {
                if($post['TYPE'] == 'get') {
                    $this->getUserDiscount();
                    if (!$this->arResult['COUPON']) {
                        $this->generateDiscount();
                        if ($this->arResult['COUPON']) {
                            $this->addUserDiscount($this->arResult['COUPON']);
                        }
                    }
                }
                if($post['TYPE'] == 'check' && $post['COUPON']) {
                    $this->checkUserCoupon($post['COUPON']);
                }

                $APPLICATION->RestartBuffer();
                echo \Bitrix\Main\Web\Json::encode($this->arResult);
                die();
            }
        }
    }

    /**
     * @override
     * @throws Exception
     */
    protected function generateDiscount()
    {
        $saleDiscountId = $this->getDiscountId();

        $type = DiscountCouponTable::TYPE_ONE_ORDER;
        $this->arResult['COUPON'] = '';
        if ($saleDiscountId) {
            $coupon = DiscountCouponTable::generateCoupon(true);
            $addFields = [
                'DISCOUNT_ID' => $saleDiscountId,
                'COUPON' => $coupon,
                'TYPE' => $type,
                'MAX_USE' => 1,
                'USER_ID' => $this->arParams['USER_ID'],
                'DESCRIPTION' => '',
            ];

            $today = new \Bitrix\Main\Type\DateTime;
            $addFields['ACTIVE_FROM'] = clone($today);
            $addFields['ACTIVE_TO'] = $today->add("3 hours");

            $addDb = DiscountCouponTable::add($addFields);
            if ($addDb->isSuccess()) {
                $this->arResult['COUPON'] = $coupon;
                if ($this->isNewDiscount) {
                    CSaleDiscount::Update($saleDiscountId, array('ACTIVE' => 'Y'));
                }
            }
        }
    }

    /**
     * @return int
     */
    protected function getDiscountId()
    {
        global $USER;
        $discountId = null;
        $xmlId = $this->arParams['DISCOUNT_XML_ID'];
        $saleDiscountValue = (float)$this->arParams['DISCOUNT_VALUE'];
        $saleDiscountUnit = (string)$this->arParams['DISCOUNT_UNIT'];
        $siteId = $this->getSiteId();
        if ($xmlId == '' && $saleDiscountValue > 0 && $saleDiscountUnit <> '') {
            $xmlId = "user_" . $saleDiscountValue . "_" . $saleDiscountUnit;
        }

        $fieldsAdd = [
            'LID' => $siteId ? $siteId : CSite::GetDefSite(),
            'NAME' => Loc::getMessage("DF_DISCOUNT_NAME"),
            'ACTIVE' => 'Y',
            'ACTIVE_FROM' => '',
            'ACTIVE_TO' => '',
            'PRIORITY' => 1,
            'SORT' => 100,
            'LAST_DISCOUNT' => 'Y',
            'XML_ID' => $xmlId,
            'USER_GROUPS' => [6],
            'ACTIONS' => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => ['All' => 'AND'],
                'CHILDREN' => [
                    [
                        'CLASS_ID' => 'ActSaleBsktGrp',
                        'DATA' => [
                            'Type' => 'Discount',
                            'Value' => $saleDiscountValue,
                            'Unit' => $saleDiscountUnit,
                            'All' => 'AND',
                            'Max' => '0',
                            'True' => 'True'
                        ],
                        'CHILDREN' => []
                    ]
                ]
            ],
            'CONDITIONS' => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => [
                    'All' => 'AND',
                    'True' => 'True',
                ],
                'CHILDREN' => []
            ]
        ];

        if ($xmlId == '') {
            return null;
        }

        $fields = [
            'XML_ID' => $xmlId,
            'ACTIVE' => 'Y'
        ];
        $saleDiscountData = DiscountTable::getList([
            'filter' => $fields,
            'select' => ['ID', 'ACTIONS', 'CONDITIONS']
        ]);
        $serializedAction = serialize($fieldsAdd['ACTIONS']);
        $serializedCondition = serialize($fieldsAdd['CONDITIONS']);
        while ($saleDiscount = $saleDiscountData->fetch()) {
            if ($saleDiscount['ACTIONS'] == $serializedAction && $saleDiscount['CONDITIONS'] == $serializedCondition) {
                $discountId = $saleDiscount['ID'];
            }
        }

        if (!$discountId) {
            $fieldsAdd['ACTIVE'] = 'N';
            $discountId = CSaleDiscount::Add($fieldsAdd);
            $this->isNewDiscount = true;
        }

        return $discountId;
    }

    /**
     * Start Component
     */
    public function executeComponent()
    {
        try {
            $this->checkModules();
            $this->prepareData();
            $this->includeComponentTemplate();
        } catch (SystemException $e) {
            ShowError($e->getMessage());
        }
    }
}