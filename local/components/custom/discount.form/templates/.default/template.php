<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
$formGetId = 'form_get_' . CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$formCheckId = 'form_check_' . CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
?>
<form action="" method="POST" class="mb-4" id="<?=$formGetId?>">
    <?=bitrix_sessid_post()?>
    <div class="form-group row">
        <div class="col-8">
            <div class="p-3 mb-2 bg-light text-dark rounded js-info font-weight-bold"></div>
        </div>
        <div class="col-4">
            <input type="submit" name="submit" value="Получить скидку" class="btn btn-primary col-12">
        </div>
    </div>
</form>
<form action="" method="POST" id="<?=$formCheckId?>">
    <?=bitrix_sessid_post()?>
    <div class="form-group row">
        <div class="col-8">
            <input type="text" name="check_coupon" class="form-control mb-2" value="">
            <div class="p-3 mb-2 bg-light text-dark rounded js-info font-weight-bold"></div>
        </div>
        <div class="col-4">
            <input type="submit" name="submit" value="Проверить скидку" class="btn btn-primary col-12">
        </div>
    </div>
</form>

<script>
    document.addEventListener('DOMContentLoaded', () => {
        function showInfo(form,json) {
            let text = '';
            if(json.DISCOUNT_TEXT) {
                text += json.DISCOUNT_TEXT;
            }
            if(json.COUPON_TEXT) {
                text += '\n'+json.COUPON_TEXT;
            }
            if(json.ERRORS) {
                text = json.ERRORS;
            }
            form.querySelector('.js-info').innerText = text;
        }

        const <?=$formGetId?> = document.getElementById('<?=$formGetId?>');
        <?=$formGetId?>.onsubmit = (e) => {
            let form = <?=$formGetId?>;
            fetch(window.location.href, {
                method: 'post',
                body: JSON.stringify({
                    AJAX: 'Y',
                    TYPE: 'get'
                })
            })
                .then(res => res.json())
                .then(json => showInfo(form,json));
            e.preventDefault();
        };

        const <?=$formCheckId?> = document.getElementById('<?=$formCheckId?>');
        <?=$formCheckId?>.onsubmit = (e) => {
            let form = <?=$formCheckId?>;
            let data = new FormData(form);
            fetch(window.location.href, {
                method: 'post',
                body: JSON.stringify({
                    AJAX: 'Y',
                    TYPE: 'check',
                    COUPON: data.get('check_coupon')
                })
            })
                .then(res => res.json())
                .then(json => showInfo(form,json));
            e.preventDefault();
        };
    });
</script>