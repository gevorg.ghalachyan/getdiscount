<?
$MESS["DF_IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Инфоблоки не установлен";
$MESS["DF_CATALOG_MODULE_NOT_INSTALLED"] = "Модуль Каталогов не установлен";
$MESS["DF_SALE_MODULE_NOT_INSTALLED"] = "Модуль магазина не установлен";
$MESS["DF_DISCOUNT_NAME"] = "Сгенерирован по форме получить скидку";
$MESS["DF_DISCOUNT_TEXT"] = "Ваша скидка ";
$MESS["DF_COUPON_TEXT"] = "Ваш купон ";
$MESS["DF_COUPON_DELETED"] = "Купон уже удален";
$MESS["DF_IBLOCK_ITEM_PRE_NAME"] = "Скидка для пользователя по ID ";
$MESS["DF_IBLOCK_COUPON_ERROR"] = "Скидка недоступна";
?>